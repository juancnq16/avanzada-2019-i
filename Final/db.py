import sqlite3
def consultaCuentas():
    con = sqlite3.connect('Final/cajero.db')
    cursorObj = con.cursor()
    cursorObj.execute("SELECT * FROM Cuenta")
    filas = cursorObj.fetchall()
    return filas
def consultaTransaccion(idCliente):
    con = sqlite3.connect('cajero.db')
    cursorObj = con.cursor()
    #-- sentencia para consultar la transaccion de un cliente
    sentencia="SELECT monto_retirado from Transaccion,cliente,Cuenta, Cuenta_Transaccion "
    sentencia+="where identificacion_cliente=idCliente "
    sentencia +="and identificacion_transaccion=idTransaccion "
    sentencia += "and identificacion_cuenta=idCuenta "
    sentencia += "and identificacion_cliente= "
    sentencia=sentencia+str(idCliente)
    cursorObj.execute(sentencia)
    resultado=cursorObj.fetchall()
    print(resultado)
def saldoCliente(idCliente):
    con = sqlite3.connect('cajero.db')
    cursorObj = con.cursor()
#   --sentencia para consultar saldo de un Cliente
    sentencia = "select saldo from Cuenta,Cliente "
    sentencia += "where identificacion_cliente=idCliente "
    sentencia += "and identificacion_cliente= "
    sentencia += str(idCliente)
    cursorObj.execute(sentencia)
    resultado = cursorObj.fetchall()
    res=resultado[0][0]
    print(res)
    return int(res)
def crearCliente(idCliente, nombre):
    #--sentencia para crear un Cliente
    con = sqlite3.connect('cajero.db')
    cursorObj = con.cursor()
    sentencia = "insert into Cliente (idCliente, nombre) VALUES ("
    sentencia +=  str(idCliente)+","
    sentencia += "'"+str(nombre)+"'"+")"
    print(sentencia)
    cursorObj.execute(sentencia)
    con.commit()
def consultaAdmin(id,nome):
    con = sqlite3.connect('cajero.db')
    cursorObj = con.cursor()
    sentencia = "SELECT * FROM Admin"
    cursorObj.execute(sentencia)
    admins=cursorObj.fetchall()
    print(admins)
    for admin in admins:
        if admin[0]==id and admin[1]==nome:
            return True
    print(admins)
    return False
def eliminarCliente(id):
    con = sqlite3.connect('cajero.db')
    cursorObj = con.cursor()
    #-- sentencia para elinar un Cliente
    sentencia = "DELETE from Cliente WHERE idCliente = "
    sentencia += str(id)
    cursorObj.execute(sentencia)
    con.commit()
def consultaUsuario(id, nombre):
    con = sqlite3.connect('cajero.db')
    cursorObj = con.cursor()
    sentencia = "SELECT * FROM Cliente"
    cursorObj.execute(sentencia)
    users = cursorObj.fetchall()
    for user in users:
        if user[0]==id and user[1]==nombre:
            return True
    return False
def retirarSaldo(idCuenta, monto):

    con = sqlite3.connect('cajero.db')
    cursorObj = con.cursor()
    saldo = saldoCliente(obtenerCliente(idCuenta))
    sentencia = "UPDATE Cuenta SET saldo = "
    if saldo>monto:
        saldo = saldo - monto
        sentencia+=str(saldo)
        sentencia += " WHERE idCuenta = "
        #
        sentencia += str(idCuenta)
        #))
        print("------",sentencia)
        cursorObj.execute(sentencia)
        con.commit()
        return True
    else:
        return False
def obtenerCuenta(idCliente):
    con = sqlite3.connect('cajero.db')
    cursorObj = con.cursor()
    sentencia = "SELECT * FROM Cuenta WHERE identificacion_cliente = "
    sentencia+=str(idCliente)
    print(sentencia)
    cursorObj.execute(sentencia)
    resultados = cursorObj.fetchall()
    return resultados[0][0]
def obtenerCliente(idCuenta):
    con = sqlite3.connect('cajero.db')
    cursorObj = con.cursor()
    sentencia = "SELECT * FROM Cuenta WHERE idCuenta = "
    sentencia+=str(idCuenta)
    print(sentencia)
    cursorObj.execute(sentencia)
    resultados = cursorObj.fetchall()
    return resultados[0][1]
    
#consultaTransaccion(1010)
#saldoCliente(1012)
#res=crearCliente(1014, "juan")
#print(res)
#print(consultaAdmin(1,'Juan'))
#eliminarCliente(1080)
#print(consultaUsuario(1019,"Luis"))
#retirarSaldo(5010, 1)