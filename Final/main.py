import sys, random, json
from flask import Flask, render_template, request, redirect, Response, make_response, jsonify
import db
app= Flask(__name__)

@app.route("/")
def output():
    filas = db.consultaCuentas()
    print(filas[0])
    return render_template("index.html")
@app.route("/principal")
def principal():
    return render_template("principal.html")
@app.route("/cliente")
def cliente():
    return render_template("cliente.html")
@app.route("/administrador")
def administrador():
    return render_template("administrador.html")
@app.route("/creaUsuario", methods = ['POST'])
def creaUsuario():
    datosCliente = request.form['datosCliente']
    print(datosCliente)
    datos=datosCliente.split(",")
    try:
        db.crearCliente(datos[0], datos[1])
        return "melo"
    except:
        return 'bad request!', 400
@app.route("/confirmar", methods = ['POST'])
def confirmacion():
    cadena={'respuesta':"true"}
    json_str=json.dumps(cadena)
    return json_str
@app.route("/verificar", methods = ['POST'])
def verificador():
    datosIngreso=request.form['datosIngreso']
    datosIngreso=datosIngreso.split(',')
    usuario=int(datosIngreso[0])
    nombre=datosIngreso[1]
    ingreso = db.consultaAdmin(usuario, nombre)
    if ingreso:
        return "melo"
    else:
        return 'bad request!', 400
@app.route("/eliminarCliente", methods = ['POST'])
def eliminarCliente():
    idCliente=request.form['idCliente']
    try:
        db.eliminarCliente(idCliente)
        return "melo"
    except:
        return 'bad request!', 400
@app.route("/verificarUsuario", methods =['POST'])
def verificarCliente():
    datosIngreso=request.form['datosCliente']
    datosIngreso=datosIngreso.split(",")
    idUsuario = int(datosIngreso[0])
    nombreUsuario = datosIngreso[1]
    ingreso=db.consultaUsuario(idUsuario,nombreUsuario)
    if ingreso:
        return "melo"
    else:
        return 'bad request!', 400
@app.route("/retiro", methods = ['POST'])
def retirar():
    print("llego")
    monto = request.form['monto']
    idCuenta = request.form['idCuenta']
    monto = int(monto)
    exito=db.retirarSaldo(idCuenta, monto)
    if exito:
        return "melo"
    else:
        return 'bad request!', 400
if __name__=="__main__":
    app.run(debug = True)