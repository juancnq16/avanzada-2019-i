import sys, random, json
from flask import Flask, render_template, request, redirect, Response, make_response, jsonify
import logica
app = Flask(__name__)
@app.route("/")
def output():
    return render_template("index.html", name="Juan" )

@app.route('/receptor', methods = ['POST'])
def receptor():
    datos={'fila 1':'1,2,3,4,5,6,7,8'}
    matriz = logica.generarsudoku()
    matriz = str(matriz)
    json_str = json.dumps(datos)
    print(request.get_json())
    return json_str

@app.route('/pagina')
def salida():
    return render_template("index2.html")
if __name__=="__main__":
    app.run(debug = True)
