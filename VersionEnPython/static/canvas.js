var canvas = document.querySelector("#lienzo");
var ctx = canvas.getContext("2d")

generarMalla();
dibujarNumero(0, 0, 1);
dibujarNumero(8, 0, 2);
dibujarNumero(4, 4, 3);
dibujarNumero(0, 8, 4);
dibujarNumero(8, 8, 5);


//Dibujar la malla del sudoku
function generarMalla() {
    ctx.fillStyle = "black";
    for (var i = 0; i < 9; i++) {
        for (var j = 0; j < 9; j++) {
            ctx.strokeRect(i * 100, j * 100, 100, 100);
        }
    }
    ctx.lineWidth = 5;
    ctx.beginPath();
    for (var i = 1; i < 3; i++) {
        ctx.moveTo(i*300, 0);
        ctx.lineTo(i*300, 900);
        ctx.stroke();
    }
    for (var i = 1; i < 3; i++) {
        ctx.moveTo(0, i*300);
        ctx.lineTo(900, i*300);
        ctx.stroke();
    }
}

function dibujarNumero(x, y, n) {
    ctx.font = "70px Arial";
    posX = x * 100;
    posY = y * 100;
    num = n.toString();
    ctx.fillText(num, posX + 30, posY + 75);

}
function consultar(){
    console.log("consultando con python")
    $.post("receptor")
    .then(function (response) {
        return response.text();
    })
    .then (function(text){
        console.log(text)
    })
}