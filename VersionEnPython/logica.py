from random import randint, choice
def generarsudoku():
    
    matriz=[]
    for i in range(0,9):
        matriz.append([])
        for j in range(0,9):
            matriz[i].append(0)
    matriz=llenarMatriz(matriz)
    return matriz
def llenarMatriz(matriz):
    for i in range(0,9):
        numeros=[1,2,3,4,5,6,7,8,9]
        for j in range(0,9):
            b=True
            while(b):
                aleatorio=choice(numeros)
                #print(numeros)}
                matriz[i][j]=aleatorio
                numeros.remove(int(aleatorio))
                b=False
    return matriz
def comprobar(matriz, fila, columna, num):
    for i in range(0,9):
        if matriz[fila][i]==num or matriz[i][columna]==num:
            return False
    return True
def cuadrante(matriz, num):
    for i in range(0,3):
        for j in range(0,3):
            if matriz[i][j]==num:
                return False
    return True
mat=generarsudoku()
print(mat)